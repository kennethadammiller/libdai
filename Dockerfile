FROM research_base
ARG user


USER root
RUN apt-get update && apt-get install g++ make doxygen graphviz libboost-dev libboost-graph-dev libboost-program-options-dev libboost-test-dev libgmp-dev cimg-dev swig2.0 python-dev -y
RUN echo "root:root" | chpasswd

USER ${user}
WORKDIR /home/${user}
RUN git clone https://kennethadammiller@bitbucket.org/kennethadammiller/libdai.git ; cd libdai ; cp Makefile.LINUX Makefile.conf && make -j4
